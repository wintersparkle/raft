(ns raft.schema
  (:require
   [clojure.spec.alpha :as s]
   [manifold.time :as time]))

(s/def :raft/term int?)
(s/def :raft/current-term :raft/term)
(s/def :raft/last-log-term :raft/term)
(s/def :raft/prev-log-term :raft/term)
(s/def :raft/voted-for (s/nilable uuid?))

(s/def :raft/index int?)
(s/def :raft/match-index :raft/index)
(s/def :raft/last-applied :raft/index)
(s/def :raft/next-index :raft/index)

(s/def :raft/phase #{:leader :candidate :follower})
(s/def :raft/heartbeat (s/nilable int?))
(s/def :raft/id uuid?)

(s/def :application/command (s/or :s string? :b bytes?))
(s/def :raft/log-entry (:s/keys :req [:raft/term :application/command]))
(s/def :raft/logs (s/coll-of :raft/log-entry))

(s/def :raft/state (s/keys
                    :opt [:raft/voted-for]
                    :req [:raft/current-term
                          :raft/logs
                          :raft/commit-index
                          :raft/last-applied]))

(def default-state #:raft{:current-term 0
                          :voted-for nil
                          :logs []
                          :last-applied -1
                          :commit-index -1})
(def default-uuid (parse-uuid "00000000-0000-0000-0000-000000000000"))
(def default-context #:raft{:id default-uuid
                            :state default-state
                            :phase :follower
                            :heartbeat 0
                            :peers {}
                            :election-timeout (time/seconds 10)})

(s/valid? :raft/state default-state)
(s/valid? :raft/state (assoc default-state :raft/voted-for default-uuid))

(s/def :raft/context (s/keys :req [:raft/id :raft/state :raft/phase
                                   :raft/heartbeat :raft/peers :raft/election-timeout]))

(def check-context (partial s/valid? :raft/context))

(check-context {})
(check-context default-context)
