(ns raft.vote
  (:require
   [clojure.spec.alpha :as s]
   manifold.time
   [raft.schema :as schema]
   [raft.term :refer [check-term] :as term]))

(defn- refuse-vote?
  "given ctx and a vote request, returns the reason we can't grant the vote as a keyword"
  [{{:keys [::logs ::current-term ::voted-for]} ::state :as ctx}
   {:keys [::term
           ::candidate-id
           ::last-log-index
           ::last-log-term]}]
  {:pre [(do (s/explain ::context ctx) true) (schema/check-context ctx)]}
  (cond
    (< term current-term) :old-term
    (not (or (nil? voted-for) (= voted-for candidate-id))) :already-voted
    (not=
     (-> logs
         (get last-log-index {})
         (get :term 0)) last-log-term) :logs-not-up-to-date
    :else nil))

(defn vote-for
  "given a request, a promise to which to deliver the response, and a peer's
  context, compute the response and return the appropriately transformed
  context
  
  See https://raft.github.io/raft.pdf RequestVote RPC
  "
  [req p {{:keys [::current-term]} ::state :as ctx}]
  (let [refused (refuse-vote? ctx req)
        granted? (not refused)
        new-ctx
        (-> ctx
            (check-term req)
            (update-in
             [::state ::voted-for]
             (fn [voted-for]
               (if granted? (::candidate-id req) voted-for))))]
    (deliver p {::vote-granted? granted? ::term current-term ::why refused})
    new-ctx))

