(ns raft.election (:require
                   [clojure.test :refer [deftest is with-test]]
                   manifold.time
                   [raft.api :refer [request-vote]]
                   [raft.schema :refer [check-context default-context default-state]]
                   [raft.term :as term]))

(defn ^:dynamic *now*
  "returns current time in milliseconds; rebindable for testing"
  []
  (System/currentTimeMillis))

(defn add-vote [ctx]
  (fn
    ([] [2 ctx]) ; start with a vote (each vote is 2 counts)
    ([[total ctx]] (assoc ctx :raft/phase
                          (if (> total (count (:raft/peers ctx))) :leader :candidate)))
    ([[tally ctx] {:keys [:raft/vote-granted? :raft/term]}]
     [(cond
        vote-granted? (+ tally 2)
        :else tally) ctx])))

(deftest test-add-vote
  (let [peer1 (random-uuid)
        peer2 (random-uuid)
        peer3 (random-uuid)
        ctx (assoc default-context :raft/peers {peer1 {} peer2 {} peer3 {}})
        tally (add-vote ctx)]
    (is (= [2 ctx]  (tally)))
    (let [{phase :raft/phase} (tally [4 ctx])]
      (is (= phase :leader)))
    (let [{phase :raft/phase} (tally [2 ctx])]
      (is (= phase :candidate)))
    (is (= [4 ctx] (tally [2 ctx] {:raft/vote-granted? true :raft/term 1})))
    (is (= [2 ctx] (tally [2 ctx] {:raft/vote-granted? false :raft/term 1})))))

(defn gather-votes [{peers :raft/peers
                     {:keys [:raft/candidate-id :raft/logs :raft/current-term]} :raft/state :as ctx}]
  (let [idx (dec (count logs))]
    (->> peers
         (pmap (fn [peer]
                 (request-vote peer {:raft/term current-term
                                     :raft/candidate-id candidate-id
                                     :raft/last-log-index idx
                                     :raft/last-log-term (-> logs)
                                     (get idx {})
                                     (get :raft/term -1)})))
         (transduce ; dude i barely understand this don't ask why conj
          term/check-term-transducer add-vote ctx))))

(defn- initial-leader-state [{{:keys [:raft/logs]} :raft/state
                              peers :raft/peers}]
  (->> peers
       (map (fn [[id peer]]
              [id (merge peer {:raft/match-index 0 :raft/next-index (count logs)})]))
       (into {})))

(initial-leader-state {:raft/peers {:whatever {:a :b}}})

(with-test
  (defn run-election [{id :raft/id  :as ctx}]
    (println id "is starting election")
    (let [n (*now*)]
      (-> ctx
          (update-in [:raft/state :raft/current-term] inc) ; increment term
          (assoc-in [:raft/state :raft/voted-for] id) ; vote for yourself
          (assoc :raft/phase (gather-votes ctx)  ; ask all your peers to vote for you
                 :raft/heartbeat n)
          (as-> c (if (= (:raft/phase c) :leader)
                    (assoc c :raft/leader-state (initial-leader-state c))
                    c)))))
  (binding [*now* #(do 250)]
    (let [{{term :raft/current-term vote :raft/voted-for} :raft/state
           :keys [:raft/id :raft/phase :raft/heartbeat :raft/leader-state]}
          (run-election
           (assoc default-context
                  :raft/id (random-uuid)
                  :raft/state (assoc default-state :raft/current-term 1)
                  :raft/phase :candidate
                  :raft/heartbeat (- (*now*) 100)
                  :raft/election-timeout 50))]
      (is (= term 2))
      (is (= phase :leader))
      (is (= heartbeat (*now*)))
      (is (= vote id)))))

(defn maybe-run-election [{:keys [:raft/heartbeat
                                  :raft/election-timeout]
                           {:keys [:raft/phase]} :raft/state
                           :as ctx}]
  {:pre [check-context]
   :post [check-context]}
  (if (and (not= :leader phase) (> (- (*now*) (or heartbeat 0)) election-timeout))
    (run-election ctx) ctx))

(maybe-run-election
 (assoc default-context
        :raft/id (random-uuid)
        :raft/state default-state
        :raft/phase :candidate
        :raft/heartbeat (- (*now*) 100) :raft/election-timeout 50))


