(ns raft.api)

(defprotocol Raft
  (append [this req]) ;=> future append response 
  (request-vote [this req]))

