(ns raft.term
  (:require
   [clojure.test :refer [is with-test]]
   [raft.schema :refer [default-context]]))

(with-test
  (defn check-term
    [{{current-term :raft/current-term} :raft/state  :as ctx}
     {term :raft/term}]
    (-> ctx
        (update-in [:raft/phase]
                   (fn [phase]
                     (cond
                       (nil? term) phase
                       (> term (or current-term 0)) :follower
                       :else phase)))
        (assoc-in [:raft/state :raft/current-term] term)))
  (let [{phase :raft/phase {current-term :raft/current-term} :raft/state}
        (check-term
         {:raft/state {:raft/current-term 1} :raft/phase :candidate}
         {:raft/term 1})]
    (is (= phase :candidate))
    (is (= current-term 1)))
  (let [{phase :raft/phase {current-term :raft/current-term} :raft/state}
        (check-term
         {:raft/state {:raft/current-term 1} :raft/phase :candidate}
         {:raft/term 2})]
    (is (= phase :follower))
    (is (= current-term 2))))

(with-test
  (defn check-term-transducer [rf]
    (fn
      ([] (rf))
      ([ctx] (rf ctx))
      ([ctx response] (rf (check-term ctx response)))))
  (is (= [] ((check-term-transducer conj))))
  (is (= {} ((check-term-transducer conj) {})))
  (is (=
       {:raft/state {:raft/current-term 2} :raft/phase :follower}
       ((check-term-transducer conj)
        {:raft/state {:raft/current-term 1} :raft/phase :candidate}
        {:raft/term 2}))))

(identity #:raft{:a :b})



