(ns raft
  (:require
   [clojure.spec.alpha :as s]
   [clojure.test :refer [deftest is]]
   manifold.time
   [raft.api :refer [append Raft request-vote]]
   [raft.election :as election]
   [raft.schema :refer [default-context default-state] :as schema]
   [raft.term :refer [check-term] :as term]
   [raft.vote :as vote]))

(defn ^:dynamic *now*
  "returns current time in milliseconds; rebindable for testing"
  []
  (System/currentTimeMillis))

(defn can-append? [ctx {:keys [::term ::prev-log-term ::prev-log-index]}]
  (let [{:keys [::state]} ctx
        {:keys [::current-term ::logs]} state
        prev-log (get logs prev-log-index {})]
    (cond
      (< term current-term)
      {::success? false ::term current-term ::why :old-term}
      (not= prev-log-term (::term prev-log))
      {::why :mismatch-prev-log ::success? false ::term term ::detail prev-log}
      :else
      {::success? true ::term current-term})))

(defn- append-logs-mutations [{:keys [::state]} {:keys [::prev-log-index ::entries]}]
  (let [{:keys [::logs]} state
        matches (fn [l r] (= (:term r) (:term l)))]
    (->> (concat logs (repeat nil)) ; don't stop whwn we run out of logs
         (drop prev-log-index) ; start at prev
         (map list (concat entries (repeat nil)))
         (reduce
          (fn [{:keys [matching res]} [entry log]]
            (let [next (conj res entry)]
              (cond
                (and (nil? entry) (nil? log)) (reduced res) ; done
                (and (nil? entry) matching) {:matching matching :res (conj res log)} ; can add log; leader is replaying old messages
                (nil? log) {:matching matching :res next} ; new info from leader
                (and matching (matches log entry)) {:matching matching :res next} ; matching item; don't matter which we pick as long as we keep it, so respect leaders
                :else {:matching false :res next}))) ; we stopped matching; propagating matching false
          {:matching true :res []})
         (concat (take prev-log-index logs)))))

(defn append-mutations [{:keys [::state] :as ctx} {:keys [::leader-commit] :as req}]
  (let [new-logs (append-logs-mutations ctx req)]
    (-> ctx
        (assoc-in [::state ::logs] new-logs)
        (update-in
         [::state ::commit-index]
         #(if
           (> leader-commit (::commit-index state)) leader-commit %))
        (assoc ::heartbeat (*now*))
        (check-term req))))

(def test-heartbeat
  {::leader-id (random-uuid)
   ::term 4
   ::prev-log-term 1
   ::prev-log-index 1
   ::leader-commit 2
   ::entries []})

(def test-append
  {::leader-id (random-uuid)
   ::term 4
   ::prev-log-term 1
   ::prev-log-index 1
   ::leader-commit 2
   ::entries [{::term 4 ::delta "a"}
              {::term 4 ::delta "b"}
              {::term 4 ::delta "c"}]})

(def test-append-short
  {::leader-id (random-uuid)
   ::term 4
   ::prev-log-term 1
   ::prev-log-index 1
   ::leader-commit 2
   ::entries [{::term 4 ::delta "a"}]})

(append-mutations
 {::state (assoc default-state ::logs [{::term 1 ::delta "0"} {::term 4 ::delta "a"}])}
 test-append)

(append-mutations
 {::state (assoc default-state ::logs [{::term 1 ::delta "0"} {::term 4 ::delta "a"} {::term 4 ::delta "b"}])}
 test-append-short)

(deftest test-serve-append
  (let [{:keys [::success? ::term]}
        (can-append? {::state
                      {::current-term 2
                       ::logs [{::term 0 ::delta ""}
                               {::term 3 ::delta ""}]}} test-heartbeat)]
    (is (not success?))
    (is (= term 4)))
  (let [{:keys [::success? ::term]}
        (can-append? {::state
                      {::current-term 10
                       ::logs [{::term 0 ::delta ""}
                               {::term 2 ::delta ""}]}} test-heartbeat)]
    (is (not success?))
    (is (= term 10)))
  (let [st {::state
            {::current-term 4
             ::logs [{::term 0 ::delta ""}
                     {::term 1 ::delta ""}]}}
        {:keys [::success? ::term ::why ::detail]}
        (can-append? st test-heartbeat)]
    (is (= (::prev-log-term test-heartbeat) 1))
    (is (= (::prev-log-index test-heartbeat) 1))
    (is (= (get-in st [::state ::logs 1 ::term]) 1))
    (is (nil? why) detail)
    (is success?)
    (is (= term 4))))

(defprotocol Server
  (run [this])
  (command [this body])
  (stop [this]))

(defprotocol StateMachine (transition [this entry]))

(defn send-heartbeats [{:keys [::id ::peers]
                        {:keys [::current-term ::commit-index ::logs]} ::state :as ctx}]
  (->> peers
       (pmap
        (fn [{peer-id ::id :as peer}]
          (let [prev-index (dec (get-in peers [peer-id ::match-index]))
                prev-term (get-in logs [prev-index ::term])]
            [peer-id (append peer
                             {::leader-id id
                              ::term current-term
                              ::prev-log-term prev-term
                              ::prev-log-index prev-index
                              ::leader-commit commit-index
                              ::entries []})])))
       (reduce check-term ctx)))

(defn reduce-append-response [init]
  (fn
    ([] init)
    ([{{:keys [::current-term ::commit-index ::logs]} ::state peers ::peers :as ctx}]
     (let [candidates (range  (dec (count logs)) commit-index -1)
           qualifying-match-indices (fn [N [_ {match-index ::match-index}]] (>= match-index N))
           log-N-is-current (fn [N] (= (get-in logs [N ::term]) current-term))
           majority-match-at-least-N (fn [N]
                                       (> (* 2 (count (filter (partial qualifying-match-indices N) peers)))
                                          (count peers)))]
       (assoc-in ctx [::state ::commit-index]
                 (first (->> candidates
                             (filter log-N-is-current)
                             (filter majority-match-at-least-N))))))
    ([ctx [peer {:keys [::why ::success?]} n]]
     (cond
       (not= (::phase ctx) ::leader)
       ctx
       success?
       (-> ctx
           (update-in [::peers (:id peer) ::next-index] (partial + n))
           (update-in [::peers (:id peer) ::match-index] (partial + n)))
       (= why :mismatch-prev-log)
       (update-in ctx [::peers (:id peer) ::next-index] dec)
       :else ctx))))

(let [id1 (random-uuid)
      id2 (random-uuid)
      id3 (random-uuid)
      logs [{::term 1}
            {::term 2}
            {::term 3}
            {::term 3}
            {::term 3}]
      current-term 3
      commit-index 1
      peers {id1 {::match-index 2}
             id2 {::match-index 2}
             id3 {::match-index 2}}
      candidates (range  (dec (count logs)) commit-index -1)
      expected-new-commit-index 2
      qualifying-match-indices (fn [N [_ {match-index ::match-index}]] (>= match-index N))
      log-N-is-current (fn [N] (= (get-in logs [N ::term]) current-term))
      majority-match-at-least-N (fn [N]
                                  (> (* 2 (count (filter (partial qualifying-match-indices N) peers)))
                                     (count peers)))]
  (first (->> candidates
              (take-while log-N-is-current)
              (filter majority-match-at-least-N)))
  ((reduce-append-response {}) {::state {::logs logs ::current-term current-term ::commit-index commit-index} ::peers peers}))

(defn propagate-logs
  "propgate-logs sends logs to peers. Should only be called by leader.

    Volatile state on leaders:
    (Reinitialized after election)
    nextIndex[] for each server, index of the next log entry
    to send to that server (initialized to leader
    last log index + 1)
    matchIndex[] for each server, index of highest log entry
    known to be replicated on server
    (initialized to 0, increases monotonically

  1. peers with next-index < (count logs)
  2. send each peer an append with batch-size logs
  3. then update volatile state based on response.

  We use a slightly modified response object which includes a why key
  
  "
  ([ctx] (propagate-logs ctx 0))
  ([{{:keys [::commit-index ::logs ::current-term]}
     ::state id
     ::id peers
     ::peers :as ctx} batch-size]
   (->> peers
        (filter (fn [[_ {next-index ::next-index :as p}]]
                  (>= (dec (count logs)) next-index)))
        (pmap
         (fn [{peer-id ::id :as peer}]
           (let [prev-index (dec (get-in peers [peer-id ::match-index]))
                 next-index (get-in logs [prev-index ::next-index])
                 prev-term (get-in logs [prev-index ::term])
                 entries (take batch-size (drop next-index logs))]
             [peer
              (append peer
                      {::leader-id id
                       ::term current-term
                       ::prev-log-term prev-term
                       ::prev-log-index prev-index
                       ::leader-commit commit-index
                       ::entries entries})
              (count entries)])))
        (transduce
         term/check-term-transducer
         reduce-append-response
         ctx))))

(defn update-state-machine [{{:keys [::last-applied
                                     ::commit-index
                                     ::logs]} ::state
                             machine ::machine
                             :as ctx}]
  (or (when (> commit-index last-applied)
        (let [l (inc last-applied)]
          (when (transition machine (logs l))
            (assoc-in ctx [::state ::last-applied] l)))) ctx))

(update-state-machine
 (assoc default-context
        ::id (random-uuid)
        ::state default-state
        ::phase :candidate
        ::heartbeat (- (*now*) 100) ::election-timeout 50))

(defrecord Peer [ctx! stop!]
  Raft
  (request-vote [_ req]
    (let [p (promise)]
      (assert schema/check-context @ctx!)
      (send ctx! (vote/vote-for req p))
      p))
  (append [_ req]
    (let [p (promise)]
      (send ctx!
            (fn [ctx]
              (let [{:keys [::success?] :as res}
                    (can-append? ctx req)
                    new (when success? (append-mutations ctx req))]
                (deliver p res) (or new ctx))))
      p))

  Server
  (stop [_] (reset! stop! false) (agent-error ctx!))
  (command [_ body]
    (let [p (promise)]
      (send ctx!
            (fn [{:keys [::phase] {:keys [::voted-for ::current-term]} ::state :as ctx}]
              (condp = phase
                :leader (update-in ctx [::state ::logs]
                                   (fn [logs]
                                     (let [new-logs (conj logs {::term current-term :application/command body})]
                                       (add-watch
                                        ctx!
                                        (count new-logs)
                                        (fn [k _ _ new]
                                          (when
                                           (>= (::commit-index new) k) (deliver p {:status :ok}))))
                                       new-logs)))
                :follower (do (deliver p {:status :redirect :to voted-for}) ctx)
                :candidate (do (deliver p {:status :temporarily-unavailable}) ctx))))))
  (run [_]
    (loop [delay-by 0]
      (let [d (manifold.time/in
               delay-by
               (fn []
                 (-> ctx!
                     (send-off
                      (fn [ctx]
                        (-> ctx
                            (election/maybe-run-election)
                            (update-state-machine)))))))]
        (when-not @stop! (if-let [er (or (agent-error ctx!) @d)]
                           er
                           (recur (+ 100 (rand-int 250)))))))))
(defn new-raft-server [init-ctx]
  {:pre [(schema/check-context init-ctx)]}
  (let [ag (agent init-ctx)]
    (set-error-handler! ag (fn [_ ex] (println "error" (.getMessage ex))))
    (set-validator! ag schema/check-context)
    (->Peer ag (atom false))))

(def test-vote
  {::term 1 ::candidate-id (random-uuid) ::last-log-term 0 ::last-log-index -1})

(def peer1 (new-raft-server default-context))

(:ctx! peer1)

@(append peer1 test-heartbeat)
@(request-vote peer1 test-vote)
@(request-vote peer1 test-vote)
(:ctx! peer1)
(agent-error (:ctx! peer1))
(def f (future (run peer1)))
(stop peer1)
@f





